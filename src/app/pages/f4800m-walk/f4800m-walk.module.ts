import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { F4800mWalkPageRoutingModule } from './f4800m-walk-routing.module';

import { F4800mWalkPage } from './f4800m-walk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    F4800mWalkPageRoutingModule
  ],
  declarations: [F4800mWalkPage]
})
export class F4800mWalkPageModule {}
