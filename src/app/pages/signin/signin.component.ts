import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {

  errorOnValidate: Boolean = false;
  alertValidate: HTMLIonAlertElement;

  constructor(
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private fb: FormBuilder,
    private authService: AuthService,
    public loadingController: LoadingController
  ) { }

  userForm = this.fb.group({
    email: [''],
    password: ['']
  })

  ngOnInit() {}

  async forgotPasswordPresentAlert() {
    const alert = await this.alertCtrl.create({
      cssClass: 'forgot-password',
      header: 'Esqueceu sua senha?',
      subHeader: 'Nós enviaremos um código para o seu email para que possa redefini-la',
      inputs: [{
        type: 'text',
        name: 'email',
        placeholder: 'example@mail.com'
      }],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => this.forgotPassword(data.email)
        }
      ]
    });

    await alert.present();
  }

  async forgotPassword(email) {
    const loading = await this.loadingController.create({ message: 'Aguarde...'});
    loading.present();
    
    this.authService.forgotPassword(email).subscribe(
      response => {
        loading.dismiss();
        this.validationCodePresentAlert(email);
      },
      error => {
        loading.dismiss();
        this.authService.presentToast(error.error.message);
      }
    )
  }

  async validationCodePresentAlert(email) {
    this.alertValidate = await this.alertCtrl.create({
      cssClass: 'forgot-password',
      header: 'Código de confirmação',
      subHeader: 'Digite o código de confirmação que enviamos para o seu email.',
      inputs: [{
        type: 'text',
        name: 'code'
      }],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.validateCode(data.code, email);
            return false;
           }
        }
      ]
    });

    await this.alertValidate.present();
  }

  async validateCode(code, email) {
    const loading = await this.loadingController.create({ message: 'Aguarde...'});
    loading.present();

    const data = {code, email};
    this.authService.validateCode(data).subscribe(
      response => {
        console.log(response);
        loading.dismiss();
        this.resetPasswordPresentAlert(response);
        this.alertValidate.dismiss();
      },
      error => {
        this.errorOnValidate = true;
        loading.dismiss();
        this.authService.presentToast(error.error.message);
      }
    )
  }

  async resetPasswordPresentAlert(info) {
    const alert = await this.alertCtrl.create({
      cssClass: 'forgot-password',
      header: 'Nova senha',
      subHeader: 'Digite sua nova senha para altera-la.',
      inputs: [{
        type: 'password',
        name: 'password'
      }],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: data => this.resetPassword(data.password, info)
        }
      ]
    });

    await alert.present();
  }

  async resetPassword(password, info) {
    const loading = await this.loadingController.create({ message: 'Aguarde...'});
    loading.present();

    const data = {password, id: info.id};
    this.authService.resetPassword(data).subscribe(
      response => {
        loading.dismiss();
        this.authService.setSession(response.user, response.data.token);
        this.navigate('index');
      },
      error => {
        loading.dismiss();
        this.authService.presentToast(error.error.message);
      }
    )
  }

  async onSubmit() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });
    loading.present();
    this.authService.login(this.userForm.value).subscribe(
      response => {
        this.authService.setSession(response.user, response.data.token);
        this.navigate('index');
        loading.dismiss();
      },
      error => {
        this.authService.presentToast('login ou senha inválidos');
        loading.dismiss();
      }
    )
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
