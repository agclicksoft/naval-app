import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MBarPushUpsPage } from './m-bar-push-ups.page';

describe('MBarPushUpsPage', () => {
  let component: MBarPushUpsPage;
  let fixture: ComponentFixture<MBarPushUpsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MBarPushUpsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MBarPushUpsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
