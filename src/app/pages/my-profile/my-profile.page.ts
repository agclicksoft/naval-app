import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  days: Array<number> = [];
  months: Array<string> = [];
  years: Array<number> = [];
  user: any;
  userForm = this.fb.group({
    name: [''],
    brigade: [''],
    genre: [''],
    email: [''],
    birthday_day: [''],
    birthday_month: [''],
    birthday_year: [''],
    height: [''],
    weight: ['']
  })

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private storage: Storage,
    private authService: AuthService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.storage.get('user').then(user => {
      this.user = user;
      var dateAux = user.date_birth.split('-') 
      this.user.date_birth = new Date(dateAux[0], dateAux[1] - 1, dateAux[2], 2, 20 , 0 , 0);
      console.log(this.user.date_birth);
      this.userForm.value.name = user.name;
      this.userForm.value.brigade = user.brigade;
      this.userForm.value.genre = user.genre;
      this.userForm.value.email = user.email;
      this.userForm.value.birthday_day =  this.user.date_birth.getDate();
      this.userForm.value.birthday_month = this.user.date_birth.getMonth();
      this.userForm.value.birthday_year = this.user.date_birth.getFullYear();
      this.userForm.value.height = user.height;
      this.userForm.value.weight = user.weight;


    });
    this.setDays();
    this.setMonths();
    this.setYears();
  }

  setDays() {
    let max: number;
    this.days = [];
    this.userForm.value.birthday_month === 'Janeiro' ? max = 31
      : this.userForm.value.birthday_month === 'Fevereiro' ? max = 29
        : this.userForm.value.birthday_month === 'Março' ? max = 31
          : this.userForm.value.birthday_month === 'Abril' ? max = 30
            : this.userForm.value.birthday_month === 'Maio' ? max = 31
              : this.userForm.value.birthday_month === 'Junho' ? max = 30
                : this.userForm.value.birthday_month === 'Julho' ? max = 31
                  : this.userForm.value.birthday_month === 'Agosto' ? max = 31
                    : this.userForm.value.birthday_month === 'Setembro' ? max = 30
                      : this.userForm.value.birthday_month === 'Outubro' ? max = 31
                        : this.userForm.value.birthday_month === 'Novembro' ? max = 30
                          : this.userForm.value.birthday_month === 'Dezembro' ? max = 31
                            : max = 31;

    for (let i = 1; i <= max; i++) {
      this.days.push(i);
    }

    for (let i in this.days) {
      if (this.days[i].valueOf() == this.userForm.value.birthday_day) return
    }

    this.userForm.controls.birthday_day.setValue('')
  }

  setMonths() {
    this.months.push('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
      'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
  }

  setYears() {
    let min = new Date().getFullYear() - 18;

    for (let i = min; i >= min - 80; i--) {
      this.years.push(i);
    }
  }

  async onSubmit() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });
    loading.present();
    this.userForm.value.date_birth = `${this.userForm.value.birthday_year}-${ parseInt(this.userForm.value.birthday_month) + 1}-${this.userForm.value.birthday_day}`;
    this.userForm.value.id = this.user.id;
    this.userForm.value.height = this.parseFloat(this.userForm.value.height)
    this.userForm.value.weight = this.parseFloat(this.userForm.value.weight)
    console.log(this.userForm.value)
    this.authService.userUpdate(this.userForm.value).subscribe(
      async data => {
        await this.storage.set('user', data.data);
        console.log(this.storage.get('user'));
        this.authService.presentToast('Cadastro atualizado com sucesso.');
        loading.dismiss();
      },
      error => {
        this.authService.presentToast('Erro ao atualizar cadastro, tente novamente.');
        loading.dismiss();
      }
    )
  }

  updateDay() {
    this.setDays();
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

  parseFloat(str:string) {
    return parseFloat(str);
  }

}
