import { Component, OnInit } from '@angular/core';
import { NavController, ViewDidEnter } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-new-activity',
  templateUrl: './new-activity.page.html',
  styleUrls: ['./new-activity.page.scss'],
})
export class NewActivityPage implements OnInit, ViewDidEnter {

  timerInterval: any;
  timer: number = 6;

  constructor(
    private navCtrl: NavController,
    private geolocation: Geolocation,
  ) { }

  ngOnInit() {
    this.geolocation.getCurrentPosition();
  }

  ionViewDidEnter() {
    document.querySelectorAll('.hiddenOnCount').forEach(el => {
      el.setAttribute('style', 'display: flex;')
    })

    document.querySelectorAll('.showOnCount').forEach(el => {
      el.setAttribute('style', 'display: none;')
    })

    this.timer = 6;
  }

  startCount() {
    document.querySelectorAll('.hiddenOnCount').forEach(el => {
      el.setAttribute('style', 'display: none;')
    })

    document.querySelectorAll('.showOnCount').forEach(el => {
      el.setAttribute('style', 'display: flex;')
    })

    this.timerInterval = setInterval(() => {
      this.timer--;
      this.trackInterval(this.timerInterval);
    }, 1000)
  }

  trackInterval(timer) {
    if (this.timer === 1) {
      setTimeout(() => {
        clearInterval(timer);
        this.navCtrl.navigateForward('activity-tracker')
      }, 900);
    }
  }

  stopCount() {
    clearInterval(this.timerInterval);

    document.querySelectorAll('.hiddenOnCount').forEach(el => {
      el.setAttribute('style', 'display: flex;')
    })

    document.querySelectorAll('.showOnCount').forEach(el => {
      el.setAttribute('style', 'display: none;')
    })

    this.timer = 6;
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
