import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FAbdominalBoardPageRoutingModule } from './f-abdominal-board-routing.module';

import { FAbdominalBoardPage } from './f-abdominal-board.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FAbdominalBoardPageRoutingModule
  ],
  declarations: [FAbdominalBoardPage]
})
export class FAbdominalBoardPageModule {}
