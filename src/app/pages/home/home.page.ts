import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
  ) { }

  ngOnInit() {
    this.storage.get('user').then(
      data => {
        if (data != null) {
          this.navigate('index')
        }
      }
    )
    //   const img = document.querySelector('img');

    //   img.setAttribute('style', 'opacity: 0;');

    //   setTimeout(() => {
    //     img.setAttribute('style', 'opacity: 1;')
    //   }, 500);

    //   setTimeout(() => {
    //     img.setAttribute('style', 'opacity: 0;')
    //   }, 1500);

    //   setTimeout(() => {
    //     document.querySelector('.content').setAttribute('style', 'display: none;');
    //   }, 1900);

    //   setTimeout(() => {
    //     this.navCtrl.navigateForward('signinorsignup')
    //   }, 1940);

  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }
}
