import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { M3200mRunPageRoutingModule } from './m3200m-run-routing.module';

import { M3200mRunPage } from './m3200m-run.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    M3200mRunPageRoutingModule
  ],
  declarations: [M3200mRunPage]
})
export class M3200mRunPageModule {}
