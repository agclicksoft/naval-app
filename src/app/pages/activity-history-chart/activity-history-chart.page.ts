import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Chart } from 'chart.js';

import * as moment from 'moment'
import { ViewDidEnter } from '@ionic/angular';

@Component({
  selector: 'app-activity-history-chart',
  templateUrl: './activity-history-chart.page.html',
  styleUrls: ['./activity-history-chart.page.scss'],
})
export class ActivityHistoryChartPage implements OnInit, ViewDidEnter {

  bars: any;
  colorArray: any;

  dataByDay: Array<any> = [];
  dataByWeek: Array<any> = [];
  dataByMonth: Array<any> = [];

  dataIsEmpty: boolean = false;
  emptyMessage: string; 

  @ViewChild('barChart') barChart;

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit() {
    this.lastDaysHistoric();
    this.lastWeeksHistoric();
    this.lastMonthsHistoric();
  }

  ionViewDidEnter() {
    // this.createBarChart(['S1', 'S2'], [2.2, 5.7]);
  }
  

  segmentChanged(segment) {
    if (segment === 'day') {
      const labels = this.dataByDay.map(el => el.date)
      const data = this.dataByDay.map(el => el.distance)

      if (!labels || labels.length == 0) {
        this.dataIsEmpty = true;
        this.emptyMessage = 'Nenhum registro encontrado nos últimos dias'
      }
      else
        this.dataIsEmpty = false;

      this.createBarChart(labels, data);
    }

    else if (segment === 'week') {
      const labels = this.dataByWeek.map(el => el.date)
      const data = this.dataByWeek.map(el => el.distance)

      if (!labels || labels.length == 0) {
        this.dataIsEmpty = true;
        this.emptyMessage = 'Nenhum registro encontrado nas últimas semanas'
      }

      else
        this.dataIsEmpty = false;


      this.createBarChart(labels, data);
    }
    
    else {
      const labels = this.dataByMonth.map(el => el.date)
      const data = this.dataByMonth.map(el => el.distance)

      if (!labels || labels.length == 0) {
        this.dataIsEmpty = true;
        this.emptyMessage = 'Nenhum registro encontrado nos últimos meses'
      }
      else
        this.dataIsEmpty = false;

      this.createBarChart(labels, data);
    }
  }

  lastDaysHistoric() {
    this.commonService.getHistoryChart('day').subscribe(
      (response: Array<any>) => {
        response.map( el =>  this.dataByDay.push({date: el.date, distance: el.distance / 1000}) )
        this.segmentChanged('day');
      },
      error => {
        console.log(error)
      }
    )
  }

  lastWeeksHistoric() {
    this.commonService.getHistoryChart('week').subscribe(
      (response: Array<any>) => {
        response.map( el => {
          this.dataByWeek.push({date: el.date, distance: el.distance / 1000})
        })
      },
      error => {
        console.log(error)
      }
    )
  }

  lastMonthsHistoric() {
    this.commonService.getHistoryChart('month').subscribe(
      (response: Array<any>) => {
        response.map( el => {
          this.dataByMonth.push({date: el.date, distance: el.distance / 1000})
        })
      },
      error => {
        console.log(error)
      }
    )
  }

  createBarChart(labels, data) {
    Chart.defaults.global.defaultFontColor = '#FFFFFF';

    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          label: "Visualizações em Km's",
          data: data,
          barThickness: 39,
          maxBarThickness: 39,
          backgroundColor: '#F3AC04', // array should have same number of elements as number of dataset
          borderColor: '#F3AC04',// array should have same number of elements as number of dataset
          color: "#FFF",
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              maxTicksLimit: 5
            },
            gridLines: {
              offsetGridLines: false,
              color: "rgba(255, 255, 255, 0.1)"
            }
          }],
          xAxes: [{
            barPercentage: 0.75,
            gridLines: {
              offsetGridLines: false,
              color: "rgba(255, 255, 255, 0.1)"
            }
          }]
        },
        legend: {
          labels: {
            fontColor: '#FFFFFF',
            defaultFontColor: '#FFFFFF'
          }
        }
      }
    });
  }
}
