import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { M3200mRunPage } from './m3200m-run.page';

describe('M3200mRunPage', () => {
  let component: M3200mRunPage;
  let fixture: ComponentFixture<M3200mRunPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M3200mRunPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(M3200mRunPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
