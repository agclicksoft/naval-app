import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MAbdominalBoardPage } from './m-abdominal-board.page';

const routes: Routes = [
  {
    path: '',
    component: MAbdominalBoardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MAbdominalBoardPageRoutingModule {}
