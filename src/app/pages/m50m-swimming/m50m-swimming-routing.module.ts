import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { M50mSwimmingPage } from './m50m-swimming.page';

const routes: Routes = [
  {
    path: '',
    component: M50mSwimmingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class M50mSwimmingPageRoutingModule {}
