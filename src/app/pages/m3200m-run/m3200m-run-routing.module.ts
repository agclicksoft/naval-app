import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { M3200mRunPage } from './m3200m-run.page';

const routes: Routes = [
  {
    path: '',
    component: M3200mRunPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class M3200mRunPageRoutingModule {}
