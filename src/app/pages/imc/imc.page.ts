import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-imc',
  templateUrl: './imc.page.html',
  styleUrls: ['./imc.page.scss'],
})
export class ImcPage implements OnInit {
  imc: number;
  description: string;
  constructor(
    private navCtrl: NavController,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('user').then(
      user => {
        this.imc = user.weight / (user.height * user.height);
        if (this.imc < 18.5) 
          this.description = 'ABAIXO DO PESO';
        if (this.imc >= 18.5 && this.imc <= 24.9)
          this.description = 'PESO NORMAL'
        if (this.imc >= 25.0 && this.imc <= 29.9)
          this.description = 'SOBREPESO'
        if (this.imc >= 30.0 && this.imc <= 34.9)
          this.description = 'OBESIDADE GRAU I'
        if (this.imc >= 35.0 && this.imc <= 39.9)
          this.description = 'OBESIDADE GRAU II'
        if (this.imc > 40.0) 
          this.description = 'OBESIDADE GRAU III';
      }
    )
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
