import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FAbdominalBoardPage } from './f-abdominal-board.page';

describe('MAbdominalBoardPage', () => {
  let component: FAbdominalBoardPage;
  let fixture: ComponentFixture<FAbdominalBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FAbdominalBoardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FAbdominalBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
