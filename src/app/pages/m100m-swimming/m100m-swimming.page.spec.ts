import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { M100mSwimmingPage } from './m100m-swimming.page';

describe('M100mSwimmingPage', () => {
  let component: M100mSwimmingPage;
  let fixture: ComponentFixture<M100mSwimmingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M100mSwimmingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(M100mSwimmingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
