import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityTrackerPageRoutingModule } from './activity-tracker-routing.module';

import { ActivityTrackerPage } from './activity-tracker.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Network } from '@ionic-native/network/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityTrackerPageRoutingModule
  ],
  declarations: [ActivityTrackerPage],
  providers: [
    Geolocation,

  ]
})
export class ActivityTrackerPageModule {}
