import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { F3200mRunPage } from './f3200m-run.page';

const routes: Routes = [
  {
    path: '',
    component: F3200mRunPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class F3200mRunPageRoutingModule {}
