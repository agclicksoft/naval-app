import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { map, catchError, switchMap } from 'rxjs/operators';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor (
      private storage: Storage,
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.storage.get('token'))
      .pipe(
        switchMap(token => {
          if (request.url.indexOf(environment.API) != -1 && token) {
            request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
          }

          return next.handle(request);
        })
      )

    
    // this.storage.get('token').then(
    //   response => {
    //     if (request.url.indexOf(environment.API) != -1 && response) {
    //       const cloned = request.clone({ headers: request.headers.set('Authorization', `Bearer ${response}`) });
    //       return next.handle(cloned);
    //     }
    //     else {
    //       return next.handle(request);
    //     }
    //   },
    //   error => {
    //     console.log(error)
    //   }
    // )
    // return next.handle(request)
  }
}
