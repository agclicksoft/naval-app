import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-abdominal-perimeter',
  templateUrl: './abdominal-perimeter.page.html',
  styleUrls: ['./abdominal-perimeter.page.scss'],
})
export class AbdominalPerimeterPage implements OnInit {

  perimeter: number;
  isAvaliable: boolean = false;
  historicMode: boolean = false;
  perimeters: any;
  user: any;

  constructor(
    private authService: AuthService,
    public loadingController: LoadingController,
    private storage: Storage,
    public alertController: AlertController
  ) { }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'profalert',
      message: `
      <div id="center">
      <h5>TABELA</h5>
      <h5 class="nbr">PERÍMETRO ABDOMINAL</h5>
      <table>
      <tr>
        <th >Risco</th>
        <th>M</th>
        <th>H</th>
      </tr>
      <tr>
        <td>Aumentado</td>
        <td class="nbr"> > 80 cm </td>
        <td class="nbr"> > 94 cm </td>
      </tr>
      <tr>
        <td >Aumentado substancialmente</td>
        <td class="nbr"> > 88 cm </td>
        <td class="nbr"> >102 cm </td>
      </tr>
    </table>
    <ion-row class="mb-8px">
    <div class="full-width">
      <h2 class="ion-text-center font-14px"> Fonte: Organização Mundial da Saúde (OMS) </h2>
      
    </div>
  </ion-row>
    </div>`,
      buttons: ['OK']
    });

    await alert.present();
  }
  ngOnInit() {
    this.storage.get('user').then(user => this.user = user);
    this.getAllPerimeters();
  }

  async onSubmit() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });
    this.authService.registerPerimiter({ value: this.perimeter }).subscribe(
      data => {
        loading.dismiss();
        this.authService.presentToast('Dados salvos com sucesso.');
      },
      error => {
        loading.dismiss();
        this.authService.presentToast('Erro ao tentar salvar dados.');
      }
    )
    this.isAvaliable = true;
  }
  async getAllPerimeters() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });
    this.authService.getAllPerimeters().subscribe(
      data => {
        console.log(data)
        loading.dismiss();
        this.perimeters = data;
      },
      error => {
        loading.dismiss();
        this.authService.presentToast('Erro ao buscar dados.');
      }
    )
  }
  getMonthExtension(date) {
    const month = new Date(date).getMonth() + 1
    switch (month) {
      case 1:
        return 'Janeiro';
      case 2:
        return 'Fevereiro';
      case 3:
        return 'Março';
      case 4:
        return 'Abril';
      case 5:
        return 'Maio';
      case 6:
        return 'Junho';
      case 7:
        return 'Julho';
      case 8:
        return 'Agosto';
      case 9:
        return 'Setembro';
      case 10:
        return 'Outubro';
      case 11:
        return 'Novembro';
      case 12:
        return 'Dezembro';
      default:
        return 'Mês inexistente';
    }
  }
}
