import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FGroundPushUpsPage } from './f-ground-push-ups.page';

describe('FGroundPushUpsPage', () => {
  let component: FGroundPushUpsPage;
  let fixture: ComponentFixture<FGroundPushUpsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FGroundPushUpsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FGroundPushUpsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
