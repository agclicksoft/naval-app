import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbdominalPerimeterPage } from './abdominal-perimeter.page';

const routes: Routes = [
  {
    path: '',
    component: AbdominalPerimeterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AbdominalPerimeterPageRoutingModule {}
