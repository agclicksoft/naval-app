import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MGroundPushUpsPage } from './m-ground-push-ups.page';

describe('MGroundPushUpsPage', () => {
  let component: MGroundPushUpsPage;
  let fixture: ComponentFixture<MGroundPushUpsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MGroundPushUpsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MGroundPushUpsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
