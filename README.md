
![picture](src/assets/images/marinha.svg)

# Projeto: Marinha TFT
Condicionamento físico para homens e mulheres.  
---

```
Iniciado em 19 de jun de 2020.
Ultima atualização: 20/10/2021
```


----
## Pré-requisitos
- Node.js
- NPM
- [Ionic CLI](https://ionicframework.com/docs/intro/cli)
- Android Studio ou Xcode(MacOs), juntamente com seus devidos emuladores
- Um editor, recomendado o VS Code


## Como rodar o projeto

Siga os passos abaixo para conseguir rodar o projeto e visualizar suas funcionalidades.

### Clone

Clone este repositório e entre na pasta raiz.

```
git clone https://Willian_Medeiros@bitbucket.org/agclicksoft/naval-app.git
```

### Instalando dependências

```
$ npm install
```

### Startando aplicação

####  -- Para rodar a aplicação no navegador:

```
$ ionic serve
```
ou

#### -- Para rodar no emulador siga os passos abaixo:  

Adicione a plaforma: 

```
$ ionic cordova platform add android
```
ou 
```
$ ionic cordova platform add ios
```

#### -- Executando

```
$ ionic cordova emulate android
```
ou 

```
$ ionic cordova emulate ios
```

obs: Caso queira aplicar o autoreload rode:
```
$ ionic cordova emulate android -l
```


#### -- Obs: Caso queira remover uma plataforma:  
```
$ ionic cordova platform rm [<platform>]
```
exemplo:
```
$ ionic cordova platform rm android
```


## Gerando o APK ou IPA(iOs)
### - Alteração de versão
- Entre no arquivo "config.xml" que está na raíz do projeto
- Na linha 2 altere o valor de "android-versionCode" e "version"

### - Gerar bild
```
$ ionic cordova build android --prod --release
```
ou
```
$ ionic cordova build ios --prod
```

- Esses comandos irão gerar a bild (no android com o nome "app-release-unsigned.apk"), dentro da pasta da plataforma, no caso do android, dentro de "naval-app\platforms\android\app\build\outputs\apk\release".

## Assinar APK 
- rode o comando:

```
$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore NAVAL-APP.keystore ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk seu-app
```
```
$ A senha é: inicio@1
```
### -- Otimizar o APK
Séra preciso otimizar o apk para poder subir para a loja  

- Va até a pasta onde está instalado o SDK do android, e entre na pasta do "build-tools" e sua respectiva versão, procure pelo arquivo chamado "zipalign.exe"

```
$ Android\Sdk\build-tools\30.0.3
```
```
$ exemplo: C:\Android\Sdk\build-tools\30.0.3
```

- Copie o arquivo "zipalign.exe" e cole na mesma pasta onde foi gerado o seu apk de produção, ou seja, dentro de "naval-app\platforms\android\app\build\outputs\apk\release"  
- Para otimizar rode o comando abaixo:


```
$ zipalign -v 4 release-unsigned.apk Naval-App.apk
```
caso ele não encontre o arquivo, rode:
```
$ ./zipalign -v 4 release-unsigned.apk Naval-App.apk
```
O nome "Naval-App" pode ser qualquer um, desde que seja com ".apk"  

- A saída será um arquivo com o nome "Naval-App.apk";  
- Se tudo ocorrer sem erro, o arquivo "Naval-App.apk" está pronto para subir.  


## Erros que possa acontecer

Caso apareça o erro relacionado ao "build-tools" na versão "31.0.0", siga as informações abaixo:  

- Entre no Android Studio e desistale a versão "31.0.0" do "build-tools" e instale a versão "30.0.3"   
- Depois remova a plataforma, adicione a plataforma novamente e siga os passos de execução ou geraçao na bild  

## Links da documentação:
- [Intalação do Ionic](https://ionicframework.com/docs/intro/cli)
- [Adicionar plataforma](https://ionicframework.com/docs/cli/commands/cordova-platform)
- [Executar no emulador](https://ionicframework.com/docs/cli/commands/cordova-emulate)
- [Gerar APK](https://ionicframework.com/docs/deployment/play-store)
- [Gerar IPA (iOS)](https://ionicframework.com/docs/deployment/app-store)

## Time

Pessoas que trabalharam ou realizaram manutenção neste projeto, respectivamente:

- [Marco Lima](https://bitbucket.org/marcoclicksoft/)
- [Willian Medeiros](https://bitbucket.org/Willian_Medeiros/)
