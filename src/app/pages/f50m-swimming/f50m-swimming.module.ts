import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { F50mSwimmingPageRoutingModule } from './f50m-swimming-routing.module';

import { F50mSwimmingPage } from './f50m-swimming.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    F50mSwimmingPageRoutingModule
  ],
  declarations: [F50mSwimmingPage]
})
export class F50mSwimmingPageModule {}
