import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { M2400mRunPage } from './m2400m-run.page';

describe('M2400mRunPage', () => {
  let component: M2400mRunPage;
  let fixture: ComponentFixture<M2400mRunPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M2400mRunPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(M2400mRunPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
