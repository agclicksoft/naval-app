import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { M2400mRunPageRoutingModule } from './m2400m-run-routing.module';

import { M2400mRunPage } from './m2400m-run.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    M2400mRunPageRoutingModule
  ],
  declarations: [M2400mRunPage]
})
export class M2400mRunPageModule {}
