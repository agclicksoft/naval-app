import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { NavController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(
    private http: HttpClient,
    private storage: Storage,
    private navCtrl: NavController,
    private toast: ToastController
  ) { }

  async presentToast(message) {
    const toast = await this.toast.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  public login(user) {
    // TODO: Loading spinner
    return this.http.post<any>(`${environment.API}/v1/auth/authenticate`, user)
  }

  public async setSession(user, token) {
    await this.storage.set('user', user);
    await this.storage.set('token', token);
  }

  public async logout() {
    await this.storage.clear();
    this.navCtrl.navigateBack('signin');
  }

  public async isLoggedIn() {
    const token = await this.storage.get('token');

    if (!token) return false

    return token
  }

  public register(user) {
    return this.http.post<any>(`${environment.API}/v1/auth/register`, user);
  }

  public userUpdate(user) {
    return this.http.put<any>(`${environment.API}/v1/users/${user.id}`, user);
  }
  public registerAcativity(activity) {
    return this.http.post<any>(`${environment.API}/v1/activities`, activity);
  }
  public getAllActivities() {
    return this.http.get<any>(`${environment.API}/v1/activities`);
  }
  public registerPerimiter(perimeter) {
    return this.http.post<any>(`${environment.API}/v1/users/abominable-perimeters`, perimeter);
  }
  public getAllPerimeters() {
    return this.http.get<any>(`${environment.API}/v1/users/abominable-perimeters`);
  }

  public forgotPassword(email) {
    return this.http.post<any>(`${environment.API}/v1/auth/forgotpassword`, {email: email})
  }
  public validateCode(data) {
    return this.http.post<any>(`${environment.API}/v1/auth/validatecode`, data)
  }
  public resetPassword(data) {
    return this.http.post<any>(`${environment.API}/v1/auth/resetpassword`, data)
  }


}
