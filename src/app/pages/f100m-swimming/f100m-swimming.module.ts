import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { F100mSwimmingPageRoutingModule } from './f100m-swimming-routing.module';

import { F100mSwimmingPage } from './f100m-swimming.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    F100mSwimmingPageRoutingModule
  ],
  declarations: [F100mSwimmingPage]
})
export class F100mSwimmingPageModule {}
