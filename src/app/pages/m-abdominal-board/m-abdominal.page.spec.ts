import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MAbdominalBoardPage } from './m-abdominal-board.page';

describe('MAbdominalBoardPage', () => {
  let component: MAbdominalBoardPage;
  let fixture: ComponentFixture<MAbdominalBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MAbdominalBoardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MAbdominalBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
