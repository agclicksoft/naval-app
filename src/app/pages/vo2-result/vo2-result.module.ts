import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Vo2ResultPageRoutingModule } from './vo2-result-routing.module';

import { Vo2ResultPage } from './vo2-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Vo2ResultPageRoutingModule
  ],
  declarations: [Vo2ResultPage]
})
export class Vo2ResultPageModule {}
