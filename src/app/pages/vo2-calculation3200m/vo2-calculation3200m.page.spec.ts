import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Vo2Calculation3200mPage } from './vo2-calculation3200m.page';

describe('Vo2Calculation3200mPage', () => {
  let component: Vo2Calculation3200mPage;
  let fixture: ComponentFixture<Vo2Calculation3200mPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vo2Calculation3200mPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Vo2Calculation3200mPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
