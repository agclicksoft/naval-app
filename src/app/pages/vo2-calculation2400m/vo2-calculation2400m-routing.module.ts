import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Vo2Calculation2400mPage } from './vo2-calculation2400m.page';

const routes: Routes = [
  {
    path: '',
    component: Vo2Calculation2400mPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Vo2Calculation2400mPageRoutingModule {}
