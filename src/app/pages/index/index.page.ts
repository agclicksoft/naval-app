import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    if (window.innerHeight > 700)
      document.querySelector('ion-grid').classList.add('ion-grid-padding')
    console.log(window.innerHeight);
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
