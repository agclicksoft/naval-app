import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { F4800mWalkPage } from './f4800m-walk.page';

const routes: Routes = [
  {
    path: '',
    component: F4800mWalkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class F4800mWalkPageRoutingModule {}
