import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MAbdominalBoardPageRoutingModule } from './m-abdominal-board-routing.module';

import { MAbdominalBoardPage } from './m-abdominal-board.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MAbdominalBoardPageRoutingModule
  ],
  declarations: [MAbdominalBoardPage]
})
export class MAbdominalBoardPageModule {}
