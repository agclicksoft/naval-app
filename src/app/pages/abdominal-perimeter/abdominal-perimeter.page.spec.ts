import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AbdominalPerimeterPage } from './abdominal-perimeter.page';

describe('AbdominalPerimeterPage', () => {
  let component: AbdominalPerimeterPage;
  let fixture: ComponentFixture<AbdominalPerimeterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbdominalPerimeterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AbdominalPerimeterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
