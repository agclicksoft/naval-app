import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vo2-result',
  templateUrl: './vo2-result.page.html',
  styleUrls: ['./vo2-result.page.scss'],
})
export class Vo2ResultPage implements OnInit {
  distance: number;
  timeAsSeconds: number;
  minutes: number;
  seconds: number;
  result: number;
  description = '';
  user: any;

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe( parametros => {
      if(parametros.distance) {
        this.distance = parseInt(parametros.distance);
      }

      if (parametros.timeAsSeconds)
        this.timeAsSeconds = parseInt(parametros.timeAsSeconds);

      if (parametros.minutes)
        this.minutes = parseInt(parametros.minutes)

      if (parametros.seconds) 
        this.seconds = parseInt(parametros.seconds)

      console.log(this.timeAsSeconds + ' / ' + this.minutes + ' / ' + this.seconds)
    });
    this.storage.get('user').then(
      user => {
        this.user = user;
        this.calculateResult();
      }
    );    
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

  calculateResult() {
    const age = this.calculateAge(this.user.date_birth);

    if (this.distance == 2400) {
      this.result = 28803.5 / ( (this.minutes*60) + this.seconds)
    }
    else if (this.distance == 3200) {
      this.result = 118.4 - (4.774 * (this.minutes + (this.seconds / 60)))
    }
    else {
      alert("ERRO")
    }

    this.result = parseFloat(this.result.toFixed(2));

    console.log("RESULT:", this.result);

    console.log(age)

    if (this.user.genre === 'FEMININO') {
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result < 24
        || age >= 30 && age <= 39.99 && this.result < 20
        || age >= 40 && age <= 49.99 && this.result < 17
        || age >= 50 && age <= 59.99 && this.result < 15
        || age >= 60 && age <= 69.99 && this.result < 13
      )
        this.description = 'Muito fraca';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 24 && this.result < 31
        || age >= 30 && age <= 39.99 && this.result >= 20 && this.result < 28
        || age >= 40 && age <= 49.99 && this.result >= 17 && this.result < 24
        || age >= 50 && age <= 59.99 && this.result >= 15 && this.result < 25
        || age >= 60 && age <= 69.99 && this.result >= 13 && this.result < 18
      )
        this.description = 'Fraca';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 31 && this.result < 38
        || age >= 30 && age <= 39.99 && this.result >= 28 && this.result < 34
        || age >= 40 && age <= 49.99 && this.result >= 24 && this.result < 31
        || age >= 50 && age <= 59.99 && this.result >= 21 && this.result < 28
        || age >= 60 && age <= 69.99 && this.result >= 18 && this.result < 24
      )
        this.description = 'Regular';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 38 && this.result < 49
        || age >= 30 && age <= 39.99 && this.result >= 34 && this.result < 45
        || age >= 40 && age <= 49.99 && this.result >= 31 && this.result < 42
        || age >= 50 && age <= 59.99 && this.result >= 28 && this.result < 38
        || age >= 60 && age <= 69.99 && this.result >= 24 && this.result < 35
      )
        this.description = 'Bom';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 49
        || age >= 30 && age <= 39.99 && this.result >= 45
        || age >= 40 && age <= 49.99 && this.result >= 42
        || age >= 50 && age <= 59.99 && this.result >= 38
        || age >= 60 && age <= 69.99 && this.result >= 35
      )
        this.description = 'Excelente';
    } 
    
    
    else 
    
    
    {
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result < 25
        || age >= 30 && age <= 39.99 && this.result < 23
        || age >= 40 && age <= 49.99 && this.result < 20
        || age >= 50 && age <= 59.99 && this.result < 18
        || age >= 60 && age <= 69.99 && this.result < 16
      )
        this.description = 'Muito fraca';

      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 25 && this.result < 34
        || age >= 30 && age <= 39.99 && this.result >= 23 && this.result < 31
        || age >= 40 && age <= 49.99 && this.result >= 20 && this.result < 27
        || age >= 50 && age <= 59.99 && this.result >= 18 && this.result < 25
        || age >= 60 && age <= 69.99 && this.result >= 16 && this.result < 23
      )
        this.description = 'Fraca';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 34 && this.result < 43
        || age >= 30 && age <= 39.99 && this.result >= 31 && this.result < 39
        || age >= 40 && age <= 49.99 && this.result >= 27 && this.result < 36
        || age >= 50 && age <= 59.99 && this.result >= 25 && this.result < 34
        || age >= 60 && age <= 69.99 && this.result >= 23 && this.result < 31
      )
        this.description = 'Regular';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 43 && this.result < 53
        || age >= 30 && age <= 39.99 && this.result >= 39 && this.result < 49
        || age >= 40 && age <= 49.99 && this.result >= 36 && this.result < 45
        || age >= 50 && age <= 59.99 && this.result >= 34 && this.result < 43
        || age >= 60 && age <= 69.99 && this.result >= 31 && this.result < 41
      )
        this.description = 'Bom';
      // tslint:disable-next-line: curly
      if (
        age >= 20 && age <= 29.99 && this.result >= 53
        || age >= 30 && age <= 39.99 && this.result >= 49
        || age >= 40 && age <= 49.99 && this.result >= 45
        || age >= 50 && age <= 59.99 && this.result >= 43
        || age >= 60 && age <= 69.99 && this.result >= 41
      )
        this.description = 'Excelente';
    }
  }
  calculateAge(dobString) {
    const dob = new Date(dobString);
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
    let age = currentYear - dob.getFullYear();
    if (birthdayThisYear > currentDate) {
      age--;
    }
    return age;
  }

}
