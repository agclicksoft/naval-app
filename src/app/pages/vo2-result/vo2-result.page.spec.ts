import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Vo2ResultPage } from './vo2-result.page';

describe('Vo2ResultPage', () => {
  let component: Vo2ResultPage;
  let fixture: ComponentFixture<Vo2ResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vo2ResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Vo2ResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
