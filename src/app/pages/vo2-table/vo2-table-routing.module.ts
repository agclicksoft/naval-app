import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Vo2TablePage } from './vo2-table.page';

const routes: Routes = [
  {
    path: '',
    component: Vo2TablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Vo2TablePageRoutingModule {}
