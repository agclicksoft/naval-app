import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MBarPushUpsPageRoutingModule } from './m-bar-push-ups-routing.module';

import { MBarPushUpsPage } from './m-bar-push-ups.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MBarPushUpsPageRoutingModule
  ],
  declarations: [MBarPushUpsPage]
})
export class MBarPushUpsPageModule {}
