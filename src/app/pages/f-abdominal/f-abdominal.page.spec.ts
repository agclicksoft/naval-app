import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FAbdominalPage } from './f-abdominal.page';

describe('FAbdominalPage', () => {
  let component: FAbdominalPage;
  let fixture: ComponentFixture<FAbdominalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FAbdominalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FAbdominalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
