import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import * as moment from 'moment';

@Component({
  selector: 'app-vo2-calculation3200m',
  templateUrl: './vo2-calculation3200m.page.html',
  styleUrls: ['./vo2-calculation3200m.page.scss'],
})
export class Vo2Calculation3200mPage implements OnInit {
  value=0;
  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    const time = moment(this.value).format('HH:mm:ss');
    const timeAsSeconds = moment.duration(time).asSeconds();
    const minutes = moment(this.value).format('mm');
    const seconds = moment(this.value).format('ss');
    this.navCtrl.navigateForward(`vo2-result/3200/${timeAsSeconds}/${minutes}/${seconds}`);
  }

}
