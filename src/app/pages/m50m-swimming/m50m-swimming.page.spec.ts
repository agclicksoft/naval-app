import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { M50mSwimmingPage } from './m50m-swimming.page';

describe('M50mSwimmingPage', () => {
  let component: M50mSwimmingPage;
  let fixture: ComponentFixture<M50mSwimmingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M50mSwimmingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(M50mSwimmingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
