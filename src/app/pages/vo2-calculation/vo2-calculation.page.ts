import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-vo2-calculation',
  templateUrl: './vo2-calculation.page.html',
  styleUrls: ['./vo2-calculation.page.scss'],
})
export class Vo2CalculationPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
