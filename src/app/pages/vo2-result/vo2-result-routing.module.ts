import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Vo2ResultPage } from './vo2-result.page';

const routes: Routes = [
  {
    path: '',
    component: Vo2ResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Vo2ResultPageRoutingModule {}
