import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Vo2CalculationPage } from './vo2-calculation.page';

const routes: Routes = [
  {
    path: '',
    component: Vo2CalculationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Vo2CalculationPageRoutingModule {}
