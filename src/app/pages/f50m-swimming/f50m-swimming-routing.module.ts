import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { F50mSwimmingPage } from './f50m-swimming.page';

const routes: Routes = [
  {
    path: '',
    component: F50mSwimmingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class F50mSwimmingPageRoutingModule {}
