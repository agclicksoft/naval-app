import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FAbdominalPageRoutingModule } from './f-abdominal-routing.module';

import { FAbdominalPage } from './f-abdominal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FAbdominalPageRoutingModule
  ],
  declarations: [FAbdominalPage]
})
export class FAbdominalPageModule {}
