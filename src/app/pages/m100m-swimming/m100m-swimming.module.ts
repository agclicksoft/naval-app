import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { M100mSwimmingPageRoutingModule } from './m100m-swimming-routing.module';

import { M100mSwimmingPage } from './m100m-swimming.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    M100mSwimmingPageRoutingModule
  ],
  declarations: [M100mSwimmingPage]
})
export class M100mSwimmingPageModule {}
