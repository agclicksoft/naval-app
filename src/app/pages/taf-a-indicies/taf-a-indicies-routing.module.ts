import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TafAIndiciesPage } from './taf-a-indicies.page';

const routes: Routes = [
  {
    path: '',
    component: TafAIndiciesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TafAIndiciesPageRoutingModule {}
