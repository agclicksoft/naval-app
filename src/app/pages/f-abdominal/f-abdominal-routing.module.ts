import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FAbdominalPage } from './f-abdominal.page';

const routes: Routes = [
  {
    path: '',
    component: FAbdominalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FAbdominalPageRoutingModule {}
