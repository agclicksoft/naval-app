import { AuthService } from './../../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {

  days: Array<number> = [];
  months: Array<string> = [];
  years: Array<number> = [];

  height: string = '';

  constructor(
    private navCtrl: NavController,
    private fb: FormBuilder,
    private authService: AuthService,
    public loadingController: LoadingController
  ) { }

  userForm = this.fb.group({
    name: [''],
    brigade: [''],
    genre: [''],
    email: [''],
    password: [''],
    birthday_day: [''],
    birthday_month: [''],
    birthday_year: [''],
    height: [''],
    weight: ['']
  })

  ngOnInit() {
    this.setDays();
    this.setMonths();
    this.setYears();
  }

  setDays() {
    let max: number;
    this.days = [];
    this.userForm.value.birthday_month === 'Janeiro' ? max = 31
      : this.userForm.value.birthday_month === 'Fevereiro' ? max = 29
        : this.userForm.value.birthday_month === 'Março' ? max = 31
          : this.userForm.value.birthday_month === 'Abril' ? max = 30
            : this.userForm.value.birthday_month === 'Maio' ? max = 31
              : this.userForm.value.birthday_month === 'Junho' ? max = 30
                : this.userForm.value.birthday_month === 'Julho' ? max = 31
                  : this.userForm.value.birthday_month === 'Agosto' ? max = 31
                    : this.userForm.value.birthday_month === 'Setembro' ? max = 30
                      : this.userForm.value.birthday_month === 'Outubro' ? max = 31
                        : this.userForm.value.birthday_month === 'Novembro' ? max = 30
                          : this.userForm.value.birthday_month === 'Dezembro' ? max = 31
                            : max = 31;

    for (let i = 1; i <= max; i++) {
      this.days.push(i);
    }

    for (let i in this.days) {
      if (this.days[i].valueOf() == this.userForm.value.birthday_day) return
    }

    this.userForm.controls.birthday_day.setValue('')
  }

  setMonths() {
    this.months.push('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
      'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
  }

  setYears() {
    let min = new Date().getFullYear() - 18;

    for (let i = min; i >= min - 80; i--) {
      this.years.push(i);
    }
  }

  async onSubmit() {
    const loading = await this.loadingController.create({ message: 'Aguarde...',});
    loading.present();

    this.userForm.value.height = parseFloat(this.height.replace(',', '.'));
    this.userForm.value.date_birth = `${this.userForm.value.birthday_year}-${this.months.indexOf(this.userForm.value.birthday_month) + 1}-${this.userForm.value.birthday_day}`;
    console.log(this.userForm.value);

    this.authService.register(this.userForm.value).subscribe(
      response => {
        loading.dismiss();
        this.authService.setSession(response.user, response.data.token);
        this.navigate('index');
      },
      error => {
        this.authService.presentToast('Erro ao efetuar cadastro, tente novamente.');
        loading.dismiss();
      }
    )
  }

  updateDay() {
    this.setDays();
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }

}
