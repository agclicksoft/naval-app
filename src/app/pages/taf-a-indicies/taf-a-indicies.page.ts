import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-taf-a-indicies',
  templateUrl: './taf-a-indicies.page.html',
  styleUrls: ['./taf-a-indicies.page.scss'],
})
export class TafAIndiciesPage implements OnInit {

  body: any = '';
  genre: any = '';

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private storage: Storage
  ) { }

    tafaForm = this.fb.group({
      genre: [''],
      body:  [''],
      test: ['']
    })

  ngOnInit() {
    this.storage.get('user').then(user => {
      this.body = user.brigade;
      this.genre = user.genre;
    });
  }

  changeGenre(genre) {
    this.genre = genre;
    console.log(genre)
    console.log(this.genre);
  }

  changeBody(body) {
    this.body = body;
  }

  onSubmit() {
    var genre: string = this.tafaForm.value.genre;
    var body = this.tafaForm.value.body;
    var test: string = this.tafaForm.value.test;

    console.log(this.tafaForm.value);

    console.log(genre)

    var path = genre.substr(0, 1).toLowerCase();

    var index: number;
    test.indexOf('4800m') != -1 ? index = test.indexOf('4800m')
      : test.indexOf('2400m') != -1 ? index = test.indexOf('2400m')
        : test.indexOf('3200m') != -1 ? index = test.indexOf('3200m')
          : test.indexOf('50m') != -1 ? index = test.indexOf('50m')
            : test.indexOf('100m') != -1 ? index = test.indexOf('100m')
              : index = -1;

    if (index != -1)
      path += test.substr(index);

      test.indexOf('solo') != -1 ? path += '-ground'
      : test.indexOf('barra') != -1 ? path += '-bar'
        : test.indexOf('Abdominal Remador') != -1 ? path += '-abdominal'
          : test.indexOf('Abdominal em Prancha Frontal') != -1 ? path += '-abdominal-board'
          : 'nothing'

      test.indexOf('Caminhada') != -1 ? path += '-walk'
        : test.indexOf('Corrida') != -1 ? path += '-run'
          : test.indexOf('Natação') != -1 ? path += '-swimming'
            : test.indexOf('Flexão') != -1 ? path += '-push-ups'
              : 'nothing'

      path = path.replace(' ', '');

      console.log(path)
    this.navigate(path);
    
  }

  navigate(path) {
    this.navCtrl.navigateForward(path);
  }
}
