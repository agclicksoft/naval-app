import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Vo2CalculationPageRoutingModule } from './vo2-calculation-routing.module';

import { Vo2CalculationPage } from './vo2-calculation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Vo2CalculationPageRoutingModule
  ],
  declarations: [Vo2CalculationPage]
})
export class Vo2CalculationPageModule {}
