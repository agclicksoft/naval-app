import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { F2400mRunPageRoutingModule } from './f2400m-run-routing.module';

import { F2400mRunPage } from './f2400m-run.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    F2400mRunPageRoutingModule
  ],
  declarations: [F2400mRunPage]
})
export class F2400mRunPageModule {}
