import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FGroundPushUpsPage } from './f-ground-push-ups.page';

const routes: Routes = [
  {
    path: '',
    component: FGroundPushUpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FGroundPushUpsPageRoutingModule {}
