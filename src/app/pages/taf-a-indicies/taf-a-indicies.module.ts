import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TafAIndiciesPageRoutingModule } from './taf-a-indicies-routing.module';

import { TafAIndiciesPage } from './taf-a-indicies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TafAIndiciesPageRoutingModule
  ],
  declarations: [TafAIndiciesPage]
})
export class TafAIndiciesPageModule {}
