import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyIndiciesPage } from './my-indicies.page';

const routes: Routes = [
  {
    path: '',
    component: MyIndiciesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyIndiciesPageRoutingModule {}
