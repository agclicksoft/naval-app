import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FGroundPushUpsPageRoutingModule } from './f-ground-push-ups-routing.module';

import { FGroundPushUpsPage } from './f-ground-push-ups.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FGroundPushUpsPageRoutingModule
  ],
  declarations: [FGroundPushUpsPage]
})
export class FGroundPushUpsPageModule {}
