import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { F2400mRunPage } from './f2400m-run.page';

const routes: Routes = [
  {
    path: '',
    component: F2400mRunPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class F2400mRunPageRoutingModule {}
