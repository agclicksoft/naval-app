import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { M4800mWalkPage } from './m4800m-walk.page';

describe('M4800mWalkPage', () => {
  let component: M4800mWalkPage;
  let fixture: ComponentFixture<M4800mWalkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M4800mWalkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(M4800mWalkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
