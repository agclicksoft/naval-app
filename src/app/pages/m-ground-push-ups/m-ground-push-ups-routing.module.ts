import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MGroundPushUpsPage } from './m-ground-push-ups.page';

const routes: Routes = [
  {
    path: '',
    component: MGroundPushUpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MGroundPushUpsPageRoutingModule {}
