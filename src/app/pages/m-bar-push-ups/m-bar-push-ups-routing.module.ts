import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MBarPushUpsPage } from './m-bar-push-ups.page';

const routes: Routes = [
  {
    path: '',
    component: MBarPushUpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MBarPushUpsPageRoutingModule {}
