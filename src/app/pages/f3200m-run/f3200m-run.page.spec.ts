import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { F3200mRunPage } from './f3200m-run.page';

describe('F3200mRunPage', () => {
  let component: F3200mRunPage;
  let fixture: ComponentFixture<F3200mRunPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3200mRunPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(F3200mRunPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
