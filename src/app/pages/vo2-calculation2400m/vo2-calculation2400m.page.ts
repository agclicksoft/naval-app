import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import * as moment from 'moment';

@Component({
  selector: 'app-vo2-calculation2400m',
  templateUrl: './vo2-calculation2400m.page.html',
  styleUrls: ['./vo2-calculation2400m.page.scss'],
})
export class Vo2Calculation2400mPage implements OnInit {

  value=0;

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  navigate(path) {
        const time = moment(this.value).format('HH:mm:ss');
        const timeAsSeconds = moment.duration(time).asSeconds();
        const minutes = moment(this.value).format('mm');
        const seconds = moment(this.value).format('ss');
        this.navCtrl.navigateForward(`vo2-result/2400/${timeAsSeconds}/${minutes}/${seconds}`);
  }
}
