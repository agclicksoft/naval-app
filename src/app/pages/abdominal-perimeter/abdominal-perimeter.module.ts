import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import {NgxMaskIonicModule} from 'ngx-mask-ionic'

import { AbdominalPerimeterPageRoutingModule } from './abdominal-perimeter-routing.module';

import { AbdominalPerimeterPage } from './abdominal-perimeter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskIonicModule,
    IonicModule,
    AbdominalPerimeterPageRoutingModule
  ],
  declarations: [AbdominalPerimeterPage]
})
export class AbdominalPerimeterPageModule {}
