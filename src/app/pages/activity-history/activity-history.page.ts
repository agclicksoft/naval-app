import { AuthService } from 'src/app/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import * as moment from 'moment';
import 'moment/locale/pt-br';
@Component({
  selector: 'app-activity-history',
  templateUrl: './activity-history.page.html',
  styleUrls: ['./activity-history.page.scss'],
})
export class ActivityHistoryPage implements OnInit {

  activities: Array<any> = [];
  //  dateteste:any
  calories = 0;
  constructor(
    private authService: AuthService,
    public loadingController: LoadingController,
  ) { }

  ngOnInit() {
    //    this.dateteste= moment().locale('pt-br').format('MMMM')
    this.getAllActivities();
  }

  async getAllActivities() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });
    this.authService.getAllActivities().subscribe(
      data => {
        this.activities = data;
        loading.dismiss();
      },
      error => {
        loading.dismiss();
      }
    );
  }

  viewMoment(date, format) {
    return moment(date).subtract('3', 'hours').locale('pt-br').format(format)
  }  
}
