import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { M4800mWalkPageRoutingModule } from './m4800m-walk-routing.module';

import { M4800mWalkPage } from './m4800m-walk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    M4800mWalkPageRoutingModule
  ],
  declarations: [M4800mWalkPage]
})
export class M4800mWalkPageModule {}
