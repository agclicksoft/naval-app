import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Vo2TablePageRoutingModule } from './vo2-table-routing.module';

import { Vo2TablePage } from './vo2-table.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Vo2TablePageRoutingModule
  ],
  declarations: [Vo2TablePage]
})
export class Vo2TablePageModule {}
