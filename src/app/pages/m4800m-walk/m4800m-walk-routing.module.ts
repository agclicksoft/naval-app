import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { M4800mWalkPage } from './m4800m-walk.page';

const routes: Routes = [
  {
    path: '',
    component: M4800mWalkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class M4800mWalkPageRoutingModule {}
