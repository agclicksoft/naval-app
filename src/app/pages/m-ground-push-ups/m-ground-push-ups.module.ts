import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MGroundPushUpsPageRoutingModule } from './m-ground-push-ups-routing.module';

import { MGroundPushUpsPage } from './m-ground-push-ups.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MGroundPushUpsPageRoutingModule
  ],
  declarations: [MGroundPushUpsPage]
})
export class MGroundPushUpsPageModule {}
