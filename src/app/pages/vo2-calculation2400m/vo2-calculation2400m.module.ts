import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Vo2Calculation2400mPageRoutingModule } from './vo2-calculation2400m-routing.module';

import { Vo2Calculation2400mPage } from './vo2-calculation2400m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Vo2Calculation2400mPageRoutingModule
  ],
  declarations: [Vo2Calculation2400mPage]
})
export class Vo2Calculation2400mPageModule {}
