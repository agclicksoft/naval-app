import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Vo2CalculationPage } from './vo2-calculation.page';

describe('Vo2CalculationPage', () => {
  let component: Vo2CalculationPage;
  let fixture: ComponentFixture<Vo2CalculationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vo2CalculationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Vo2CalculationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
