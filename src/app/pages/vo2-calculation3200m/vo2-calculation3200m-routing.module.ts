import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Vo2Calculation3200mPage } from './vo2-calculation3200m.page';

const routes: Routes = [
  {
    path: '',
    component: Vo2Calculation3200mPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Vo2Calculation3200mPageRoutingModule {}
