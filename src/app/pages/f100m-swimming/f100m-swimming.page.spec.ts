import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { F100mSwimmingPage } from './f100m-swimming.page';

describe('F100mSwimmingPage', () => {
  let component: F100mSwimmingPage;
  let fixture: ComponentFixture<F100mSwimmingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F100mSwimmingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(F100mSwimmingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
