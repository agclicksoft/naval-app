import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SigninComponent } from './pages/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'index',
    loadChildren: () => import('./pages/index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'activity-tracker',
    loadChildren: () => import('./pages/activity-tracker/activity-tracker.module').then( m => m.ActivityTrackerPageModule)
  },
  {
    path: 'new-activity',
    loadChildren: () => import('./pages/new-activity/new-activity.module').then( m => m.NewActivityPageModule)
  },
  {
    path: 'activity-history',
    loadChildren: () => import('./pages/activity-history/activity-history.module').then( m => m.ActivityHistoryPageModule)
  },
  {
    path: 'my-indicies',
    loadChildren: () => import('./pages/my-indicies/my-indicies.module').then( m => m.MyIndiciesPageModule)
  },
  {
    path: 'taf-a-indicies',
    loadChildren: () => import('./pages/taf-a-indicies/taf-a-indicies.module').then( m => m.TafAIndiciesPageModule)
  },
  {
    path: 'tips',
    loadChildren: () => import('./pages/tips/tips.module').then( m => m.TipsPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'imc',
    loadChildren: () => import('./pages/imc/imc.module').then( m => m.ImcPageModule)
  },
  {
    path: 'abdominal-perimeter',
    loadChildren: () => import('./pages/abdominal-perimeter/abdominal-perimeter.module').then( m => m.AbdominalPerimeterPageModule)
  },
  {
    path: 'heart-rate',
    loadChildren: () => import('./pages/heart-rate/heart-rate.module').then( m => m.HeartRatePageModule)
  },
  {
    path: 'vo2-calculation',
    loadChildren: () => import('./pages/vo2-calculation/vo2-calculation.module').then( m => m.Vo2CalculationPageModule)
  },
  {
    path: 'vo2-calculation2400m',
    loadChildren: () => import('./pages/vo2-calculation2400m/vo2-calculation2400m.module').then( m => m.Vo2Calculation2400mPageModule)
  },
  {
    path: 'vo2-calculation3200m',
    loadChildren: () => import('./pages/vo2-calculation3200m/vo2-calculation3200m.module').then( m => m.Vo2Calculation3200mPageModule)
  },
  {
    path: 'vo2-result/:distance/:timeAsSeconds/:minutes/:seconds',
    loadChildren: () => import('./pages/vo2-result/vo2-result.module').then( m => m.Vo2ResultPageModule)
  },
  {
    path: 'vo2-table',
    loadChildren: () => import('./pages/vo2-table/vo2-table.module').then( m => m.Vo2TablePageModule)
  },
  {
    path: 'm4800m-walk',
    loadChildren: () => import('./pages/m4800m-walk/m4800m-walk.module').then( m => m.M4800mWalkPageModule)
  },
  {
    path: 'm50m-swimming',
    loadChildren: () => import('./pages/m50m-swimming/m50m-swimming.module').then( m => m.M50mSwimmingPageModule)
  },
  {
    path: 'm3200m-run',
    loadChildren: () => import('./pages/m3200m-run/m3200m-run.module').then( m => m.M3200mRunPageModule)
  },
  {
    path: 'm100m-swimming',
    loadChildren: () => import('./pages/m100m-swimming/m100m-swimming.module').then( m => m.M100mSwimmingPageModule)
  },
  {
    path: 'm-ground-push-ups',
    loadChildren: () => import('./pages/m-ground-push-ups/m-ground-push-ups.module').then( m => m.MGroundPushUpsPageModule)
  },
  {
    path: 'm-abdominal',
    loadChildren: () => import('./pages/m-abdominal/m-abdominal.module').then( m => m.MAbdominalPageModule)
  },
  {
    path: 'm-abdominal-board',
    loadChildren: () => import('./pages/m-abdominal-board/m-abdominal-board.module').then( m => m.MAbdominalBoardPageModule)
  },
  {
    path: 'f-abdominal-board',
    loadChildren: () => import('./pages/f-abdominal-board/f-abdominal-board.module').then( m => m.FAbdominalBoardPageModule)
  },
  {
    path: 'm-bar-push-ups',
    loadChildren: () => import('./pages/m-bar-push-ups/m-bar-push-ups.module').then( m => m.MBarPushUpsPageModule)
  },
  {
    path: 'f3200m-run',
    loadChildren: () => import('./pages/f3200m-run/f3200m-run.module').then( m => m.F3200mRunPageModule)
  },
  {
    path: 'f100m-swimming',
    loadChildren: () => import('./pages/f100m-swimming/f100m-swimming.module').then( m => m.F100mSwimmingPageModule)
  },
  {
    path: 'f-ground-push-ups',
    loadChildren: () => import('./pages/f-ground-push-ups/f-ground-push-ups.module').then( m => m.FGroundPushUpsPageModule)
  },
  {
    path: 'f-abdominal',
    loadChildren: () => import('./pages/f-abdominal/f-abdominal.module').then( m => m.FAbdominalPageModule)
  },
  {
    path: 'm2400m-run',
    loadChildren: () => import('./pages/m2400m-run/m2400m-run.module').then( m => m.M2400mRunPageModule)
  },
  {
    path: 'activity-history-chart',
    loadChildren: () => import('./pages/activity-history-chart/activity-history-chart.module').then( m => m.ActivityHistoryChartPageModule)
  },
  {
    path: 'f2400m-run',
    loadChildren: () => import('./pages/f2400m-run/f2400m-run.module').then( m => m.F2400mRunPageModule)
  },
  {
    path: 'f50m-swimming',
    loadChildren: () => import('./pages/f50m-swimming/f50m-swimming.module').then( m => m.F50mSwimmingPageModule)
  },
  {
    path: 'f4800m-walk',
    loadChildren: () => import('./pages/f4800m-walk/f4800m-walk.module').then( m => m.F4800mWalkPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
