import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MAbdominalPageRoutingModule } from './m-abdominal-routing.module';

import { MAbdominalPage } from './m-abdominal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MAbdominalPageRoutingModule
  ],
  declarations: [MAbdominalPage]
})
export class MAbdominalPageModule {}
