import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { M2400mRunPage } from './m2400m-run.page';

const routes: Routes = [
  {
    path: '',
    component: M2400mRunPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class M2400mRunPageRoutingModule {}
