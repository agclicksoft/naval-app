import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MAbdominalPage } from './m-abdominal.page';

const routes: Routes = [
  {
    path: '',
    component: MAbdominalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MAbdominalPageRoutingModule {}
