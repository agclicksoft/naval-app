import {
  BackgroundGeolocation, BackgroundGeolocationEvents, BackgroundGeolocationResponse,
} from '@ionic-native/background-geolocation/ngx';
import { NgZone } from '@angular/core';
import { AuthService } from './../../auth/auth.service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Subscription } from 'rxjs';
import { LoadingController, NavController, AlertController } from '@ionic/angular';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Network } from '@ionic-native/network/ngx';



@Component({
  selector: 'app-activity-tracker',
  templateUrl: './activity-tracker.page.html',
  styleUrls: ['./activity-tracker.page.scss'],
})

export class ActivityTrackerPage implements OnInit {
  aa = 0;
  initialLongitude: any;
  initialLatitude: any;
  currentLongitude: any;
  currentLatitude: any;
  oldCurrentLongitude: any;
  oldCurrentLatitude: any;
  occurrency: any;
  map: any;

  minutes: number = 0;
  seconds: number = 0;
  hours: number = 0;

  distance: any = 0;
  timer: any;
  pace = '00:00';
  calories = 0;
  weight = 0;

  watch: Subscription;
  arraySpeedAverage: Array<number> = [];
  speedAverage = 0;
  result: any;
  options = {
    timeout: 10000,
    enableHighAccuracy: true
  };
  presision = 0;

  isPaused: boolean = false;

  startTimeForeground;
  endTimeForeground;


  constructor(
    private geolocation: Geolocation,
    private storage: Storage,
    private authService: AuthService,
    public loadingController: LoadingController,
    private navCtrl: NavController,
    public alertController: AlertController,
    private backgroundGeolocation: BackgroundGeolocation,
    private backgroundMode: BackgroundMode,
    public zone: NgZone,
    private network: Network
  ) { }


  ngOnInit() {
    this.storage.get('user').then(
      user => this.weight = parseFloat(user.weight)
    );
    this.getInitialPosition().then(data => {
      this.getCurrentePosition();
      this.backgroundMode.enable();
      this.startTimer();
    });

    this.backgroundMode.un('activate', () => {

    })
  }


  getInitialPosition() {
    this.initialLatitude = null;
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition(this.options).then((resp) => {
        this.initialLatitude = resp.coords.latitude;
        this.initialLongitude = resp.coords.longitude;
        resolve('ok');
      }).catch((error) => {
        console.log('Error getting location', error);
        reject('ok');
      });
    })

  }

  trackerPosition(latitude, longitude, speed, accuracy) {

    this.currentLatitude = latitude;
    this.currentLongitude = longitude;
    this.occurrency = speed;
    if (speed > 0.7 && accuracy < 6) {

      if (this.distance === 0) {
        this.oldCurrentLatitude = this.initialLatitude;
        this.oldCurrentLongitude = this.initialLongitude;
      }
      var result
        = parseFloat(this.getDistanceFromLatLonInKm({ lat: this.oldCurrentLatitude, lng: this.oldCurrentLongitude },
          { lat: this.currentLatitude, lng: this.currentLongitude }).toFixed(11));
      if (!isNaN(result)) {
        if (result > 0.5) {
          this.distance += result;
          this.oldCurrentLongitude = this.currentLongitude;
          this.oldCurrentLatitude = this.currentLatitude;
          this.calculeteCalories();
          this.calculatePace();
          this.getAvgSpeed();
          this.arraySpeedAverage.push(speed);
        }
      }
    }

  }

  getCurrentePosition() {

    try {


      // Background Tracking

      let config = {
        desiredAccuracy: 0,
        stationaryRadius: 20,
        distanceFilter: 10,
        debug: false,
        interval: 2000
      };

      this.backgroundGeolocation.configure(config).then(() => {
        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
          console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
          // Run update inside of Angular's zone
          this.zone.run(() => {
            this.trackerPosition(location.latitude, location.longitude, location.speed, location.accuracy);
          });
        });

      }, (err) => {

        console.log(err);

      });

      // Turn ON the background-geolocation system.
      this.backgroundGeolocation.start();


      // Foreground Tracking

      let options = {
        frequency: 1000,
        enableHighAccuracy: true
      };

      this.watch = this.geolocation.watchPosition(options).subscribe((position: Geoposition) => {
        // Run update inside of Angular's zone
        this.zone.run(() => {
          this.trackerPosition(position.coords.latitude, position.coords.longitude, position.coords.speed, position.coords.accuracy);
        });

      });
    } catch (error) {

    }
  }


  startTimer() {
    try {
      this.timer = setInterval(() => {
        if (this.seconds === 59) {
          this.seconds = -1;
          this.minutes++;
        }
        if (this.minutes === 59) {
          this.minutes = -1;
          this.hours++;
        }
        this.seconds++;
        //this.registerTravelledDistance();
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  
  }
  msToTime(duration) {
    let seconds: number = Math.floor((duration / 1000) % 60);
    let minutes: number = Math.floor((duration / (1000 * 60)) % 60);
    let hours: number = Math.floor((duration / (1000 * 60 * 60)) % 24);

    const hh = (hours < 10) ? '0' + hours : hours
    const mm = (minutes < 10) ? "0" + minutes : minutes;
    const ss = (seconds < 10) ? "0" + seconds : seconds;

    return hh + ":" + mm + ":" + ss
  }

  pauseActivity() {
    // To stop notifications
    this.watch.unsubscribe();
    clearInterval(this.timer);
    this.isPaused = true;
    this.backgroundGeolocation.finish(); // FOR IOS ONLY
    this.backgroundGeolocation.stop();
  }

  retakeActivity() {
    this.watch = this.geolocation.watchPosition(this.options).subscribe((data: any) => {
      this.trackerPosition(data.coords.latitude, data.coords.longitude, data.coords.speed, data.coords.accuracy);
    });
    this.startTimer();
    this.isPaused = false;
  }

  async registerActivity() {
    const loading = await this.loadingController.create({
      message: 'Aguarde...',
    });

    if (this.network.type !== 'none') {
      this.authService.registerAcativity({
        distance: this.distance,
        time: `${this.hours}:${this.minutes}:${this.seconds}`,
        average_speed: this.speedAverage,
        pace: this.pace,
        caloric: this.calories
      }).subscribe(
        data => {
          loading.dismiss();
          this.authService.presentToast('Dados salvos com sucesso.');
          this.navCtrl.navigateForward('new-activity');
        },
        error => {
          loading.dismiss();
          this.authService.presentToast('Erro ao cadastrar atividade, tente novamente.');
        }
      );
    } else {
      loading.dismiss();
      this.authService.presentToast('Erro, verifique sua conexão e tente novamente');
    }


  }

  getAvgSpeed() {
    for (var i = 0; i < this.arraySpeedAverage.length; i++) {
      this.speedAverage = (this.arraySpeedAverage[i] / this.arraySpeedAverage.length) * this.arraySpeedAverage.length
    }
  }

  getDistanceFromLatLonInKm(position1, position2) {

    'use strict';
    // tslint:disable-next-line: one-variable-per-declaration
    const deg2rad = function (deg) { return deg * (Math.PI / 180); },
      R = 6371,
      dLat = deg2rad(position2.lat - position1.lat),
      dLng = deg2rad(position2.lng - position1.lng),
      a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
        + Math.cos(deg2rad(position1.lat))
        * Math.cos(deg2rad(position1.lat))
        * Math.sin(dLng / 2) * Math.sin(dLng / 2),
      c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return (R * c * 1000);
  }
  calculatePace() {
    if (this.distance === 0) { return; }
    const kmDistance = this.distance * 0.001;
    const minuteAndSecund = (this.hours * 60) + this.minutes + (this.seconds / 60);
    let minute = (minuteAndSecund / kmDistance);
    let secunds = '00';

    if (this.isFloat(minute)) {
      const array = minute.toString().split('.');
      const decimal = array[1];
      minute = parseInt(array[0]);
      secunds = (parseInt(decimal) * 0.6).toString().substr(0, 2);
    }
    this.pace = `${minute}:${secunds}`;
  }
  calculeteCalories() {
    this.calories = ((this.speedAverage * 3.6) //m/s para km/h
      * this.weight * 0.0175)
      * ((this.hours * 60) + this.minutes + (this.seconds
        / 60));
  }
  isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
  }
}
