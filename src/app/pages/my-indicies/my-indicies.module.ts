import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyIndiciesPageRoutingModule } from './my-indicies-routing.module';

import { MyIndiciesPage } from './my-indicies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyIndiciesPageRoutingModule
  ],
  declarations: [MyIndiciesPage]
})
export class MyIndiciesPageModule {}
