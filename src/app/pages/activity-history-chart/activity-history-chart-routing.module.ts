import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityHistoryChartPage } from './activity-history-chart.page';

const routes: Routes = [
  {
    path: '',
    component: ActivityHistoryChartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivityHistoryChartPageRoutingModule {}
