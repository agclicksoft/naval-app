import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TafAIndiciesPage } from './taf-a-indicies.page';

describe('TafAIndiciesPage', () => {
  let component: TafAIndiciesPage;
  let fixture: ComponentFixture<TafAIndiciesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TafAIndiciesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TafAIndiciesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
