import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { F2400mRunPage } from './f2400m-run.page';

describe('F2400mRunPage', () => {
  let component: F2400mRunPage;
  let fixture: ComponentFixture<F2400mRunPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F2400mRunPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(F2400mRunPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
