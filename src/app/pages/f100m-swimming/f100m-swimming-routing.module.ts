import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { F100mSwimmingPage } from './f100m-swimming.page';

const routes: Routes = [
  {
    path: '',
    component: F100mSwimmingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class F100mSwimmingPageRoutingModule {}
