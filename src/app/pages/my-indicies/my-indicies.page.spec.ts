import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyIndiciesPage } from './my-indicies.page';

describe('MyIndiciesPage', () => {
  let component: MyIndiciesPage;
  let fixture: ComponentFixture<MyIndiciesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyIndiciesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyIndiciesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
