import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { M100mSwimmingPage } from './m100m-swimming.page';

const routes: Routes = [
  {
    path: '',
    component: M100mSwimmingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class M100mSwimmingPageRoutingModule {}
