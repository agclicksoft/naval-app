import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Vo2TablePage } from './vo2-table.page';

describe('Vo2TablePage', () => {
  let component: Vo2TablePage;
  let fixture: ComponentFixture<Vo2TablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vo2TablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Vo2TablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
