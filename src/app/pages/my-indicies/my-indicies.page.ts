import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-my-indicies',
  templateUrl: './my-indicies.page.html',
  styleUrls: ['./my-indicies.page.scss'],
})
export class MyIndiciesPage implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  navigate(path) {
    this.navCtrl.navigateForward(path)
  }

}
