import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityHistoryChartPageRoutingModule } from './activity-history-chart-routing.module';

import { ActivityHistoryChartPage } from './activity-history-chart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityHistoryChartPageRoutingModule
  ],
  declarations: [ActivityHistoryChartPage]
})
export class ActivityHistoryChartPageModule {}
