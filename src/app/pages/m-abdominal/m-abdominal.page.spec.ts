import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MAbdominalPage } from './m-abdominal.page';

describe('MAbdominalPage', () => {
  let component: MAbdominalPage;
  let fixture: ComponentFixture<MAbdominalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MAbdominalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MAbdominalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
