import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { F3200mRunPageRoutingModule } from './f3200m-run-routing.module';

import { F3200mRunPage } from './f3200m-run.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    F3200mRunPageRoutingModule
  ],
  declarations: [F3200mRunPage]
})
export class F3200mRunPageModule {}
