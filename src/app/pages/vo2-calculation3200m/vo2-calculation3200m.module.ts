import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Vo2Calculation3200mPageRoutingModule } from './vo2-calculation3200m-routing.module';

import { Vo2Calculation3200mPage } from './vo2-calculation3200m.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Vo2Calculation3200mPageRoutingModule
  ],
  declarations: [Vo2Calculation3200mPage]
})
export class Vo2Calculation3200mPageModule {}
