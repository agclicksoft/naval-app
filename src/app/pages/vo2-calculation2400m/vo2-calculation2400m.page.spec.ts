import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Vo2Calculation2400mPage } from './vo2-calculation2400m.page';

describe('Vo2Calculation2400mPage', () => {
  let component: Vo2Calculation2400mPage;
  let fixture: ComponentFixture<Vo2Calculation2400mPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Vo2Calculation2400mPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Vo2Calculation2400mPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
