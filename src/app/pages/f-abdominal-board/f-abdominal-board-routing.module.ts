import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FAbdominalBoardPage } from './f-abdominal-board.page';

const routes: Routes = [
  {
    path: '',
    component: FAbdominalBoardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FAbdominalBoardPageRoutingModule {}
