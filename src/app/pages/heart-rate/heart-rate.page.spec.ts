import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HeartRatePage } from './heart-rate.page';

describe('HeartRatePage', () => {
  let component: HeartRatePage;
  let fixture: ComponentFixture<HeartRatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeartRatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HeartRatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
