import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private http: HttpClient
  ) { }

  getHistoryChart(period) {
    return this.http.get(`${environment.API}/v1/reports/activities?period=${period}`)
  }
}
