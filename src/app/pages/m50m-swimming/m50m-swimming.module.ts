import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { M50mSwimmingPageRoutingModule } from './m50m-swimming-routing.module';

import { M50mSwimmingPage } from './m50m-swimming.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    M50mSwimmingPageRoutingModule
  ],
  declarations: [M50mSwimmingPage]
})
export class M50mSwimmingPageModule {}
