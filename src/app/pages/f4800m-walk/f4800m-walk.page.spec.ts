import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { F4800mWalkPage } from './f4800m-walk.page';

describe('F4800mWalkPage', () => {
  let component: F4800mWalkPage;
  let fixture: ComponentFixture<F4800mWalkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F4800mWalkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(F4800mWalkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
