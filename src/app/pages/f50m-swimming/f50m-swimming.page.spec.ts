import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { F50mSwimmingPage } from './f50m-swimming.page';

describe('F50mSwimmingPage', () => {
  let component: F50mSwimmingPage;
  let fixture: ComponentFixture<F50mSwimmingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F50mSwimmingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(F50mSwimmingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
