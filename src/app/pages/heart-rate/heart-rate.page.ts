import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-heart-rate',
  templateUrl: './heart-rate.page.html',
  styleUrls: ['./heart-rate.page.scss'],
})
export class HeartRatePage implements OnInit {
  age = 0;
  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.storage.get('user').then(
      user => {
        if (user == null || user.date_birth === undefined) {
          this.authService.presentToast('Complete seu cadastro!');
          this.navCtrl.navigateForward('my-profile');
        }
        this.age = 220 - this.calculateAge(user.date_birth);
        if (isNaN(this.age)) {
          this.authService.presentToast('Complete seu cadastro!');
          this.navCtrl.navigateForward('my-profile');
        }
      }
    )
  }

calculateAge(dobString) {
  const dob = new Date(dobString);
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
  let age = currentYear - dob.getFullYear();
  if (birthdayThisYear > currentDate) {
    age--;
  }
  return age;
}

}
