import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewActivityPage } from './new-activity.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Network } from '@ionic-native/network/ngx';

const routes: Routes = [
  {
    path: '',
    component: NewActivityPage
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    Geolocation,
    Network
  ],
})
export class NewActivityPageRoutingModule {}
